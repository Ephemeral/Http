#include "Nagoy.hpp"
#include <winsock2.h>
#include <iphlpapi.h>
#include <IPTypes.h>
#include <sstream>
#pragma comment(lib,"iphlpapi")
namespace NagoySecondNameSpace {

	//static void PrintMACaddress(unsigned char MACData[])
	//{
	//	printf("MAC Address: %02X-%02X-%02X-%02X-%02X-%02X\n",
	//		MACData[0], MACData[1], MACData[2], MACData[3], MACData[4], MACData[5]);
	//}

	// Fetches the MAC address and prints it
	void GetMACaddress(std::vector<std::string>& adapters) {
		IP_ADAPTER_ADDRESSES AdapterInfo[16];       // Allocate information for up to 16 NICs
		DWORD dwBufLen = sizeof(AdapterInfo);       // Save memory size of buffer

		// Arguments for GetAdapterAddresses:
		DWORD dwStatus = GetAdaptersAddresses(0, 0, NULL, AdapterInfo, &dwBufLen);
		// [out] buffer to receive data
		// [in] size of receive data buffer
							// Verify return value is valid, no buffer overflow
		PIP_ADAPTER_ADDRESSES pAdapterInfo = AdapterInfo;           // Contains pointer to current adapter info

		do {
			char adapter[32] = { 0 };
			
			sprintf_s(adapter, "%02x-%02x-%02x-%02x-%02x-%02x",
				pAdapterInfo->PhysicalAddress[0],
				pAdapterInfo->PhysicalAddress[1],
				pAdapterInfo->PhysicalAddress[2],
				pAdapterInfo->PhysicalAddress[3],
				pAdapterInfo->PhysicalAddress[4],
				pAdapterInfo->PhysicalAddress[5]);
			adapters.push_back(adapter);
			pAdapterInfo = pAdapterInfo->Next;                      // Progress through linked list
		} while (pAdapterInfo);                                       // Terminate if last adapter
	}
};

namespace NagoyNS {

	NagoyData::NagoyData() {
		data = nullptr;
		length = 0;
	}
	NagoyData::~NagoyData() {
		length = 0;
		delete[] data;
	}
	Nagoy::Nagoy() {

	}
	void Nagoy::setsession(void* session) {
		if (this->session.handle == nullptr)
			this->session.handle = session;
	}
	void Nagoy::setconnection(void* connection) {
		if (this->connection.handle == nullptr)
			this->connection.handle = connection;
	}
	void Nagoy::setrequest(void* request) {
		if (this->request.handle == nullptr)
			this->request.handle = request;
	}
	const NagoyHandles& Nagoy::getsession() {
		return session;
	}
	const NagoyHandles& Nagoy::getconnection() {
		return connection;
	}
	const NagoyHandles& Nagoy::getrequest() {
		return request;
	}
	const std::vector<std::tuple<std::string, std::string>>& Nagoy::getheaders() {
		return headers;
	}
	void Nagoy::insertheader(std::string& key, std::string& val) {
		headers.push_back(std::make_tuple(key, val));
	}
	Nagoy::~Nagoy() {

	}

	NagoyHandles::NagoyHandles() {
		handle = nullptr;
	}
	NagoyHandles::~NagoyHandles() {
		if (handle != nullptr) {
			InternetCloseHandle(handle);
			handle = nullptr;
		}
	}
	std::string RequestTypeGet::getrequesttype() {
		return std::string("GET");
	}
	std::string RequestTypePost::getrequesttype() {
		return std::string("POST");
	}


	bool nagoyrequest(const NagoyArc& ref, std::string& requestpath, RequestType& type, void*query, uint64_t querysize, bool usingssl) {
		void* request = HttpOpenRequestA(ref->getconnection().handle, type.getrequesttype().c_str(), requestpath.c_str(), 0, 0, 0, usingssl ? INTERNET_FLAG_SECURE : 0, 0);
		if (request == NULL)
			return false;
		ref->setrequest(request);
		std::string header = "";
		for (std::tuple<std::string, std::string> it : ref->getheaders()) {
			std::string key = std::get<0>(it);
			std::string val = std::get<1>(it);
			header += key + " : " + val + "\r\n";
		}
		HttpAddRequestHeadersA(request, header.c_str(), header.length(), HTTP_ADDREQ_FLAG_REPLACE);

		if (!HttpSendRequestA(request, NULL, 0,query, querysize))
			return false;
		return true;
	}
	bool nagoyrequest(const NagoyArc& ref, std::string&& requestpath, RequestType&& type, void* query, uint64_t querysize, bool usingssl) {
		return nagoyrequest(ref, requestpath, type, query, querysize, usingssl);
	}
	void destroyref(const NagoyArc& ref) {

	}

	std::unique_ptr<NagoyData> readrequest(const NagoyArc& ref) {
		std::vector<std::shared_ptr<NagoyData>> parts;
		unsigned long internalquerysize = 0;
		unsigned long readsize = 0;
		unsigned char internalbuffer[4096] = { 0 };
		unsigned long total = 0;
		do
		{
			InternetQueryDataAvailable(ref->getrequest().handle, &internalquerysize, 0, 0);

			if (!internalquerysize)
				break;

			if (internalquerysize > sizeof(internalbuffer))
				internalquerysize = sizeof(internalbuffer);

			InternetReadFile(ref->getrequest().handle, internalbuffer, internalquerysize, &readsize);

			if (readsize)
			{
				std::shared_ptr<NagoyData> data = std::make_shared<NagoyData>();
				data->length = readsize;
				total += readsize;
				data->data = new char[readsize];
				memcpy(data->data, internalbuffer, readsize);
				parts.push_back(data);
			}
		} while (readsize);
		std::unique_ptr<NagoyData> complete = std::make_unique<NagoyData>();
		complete->data = new char[total];
		for (std::shared_ptr<NagoyData> data : parts) {
			memcpy(complete->data + complete->length, data->data, data->length);
			complete->length += data->length;
		}
		return complete;
	}
	std::unique_ptr<NagoyData> readresponseheaders(const NagoyArc& ref) {
		unsigned long error = 0;
		NagoyDataArc data = std::make_unique<NagoyData>();
		unsigned long size = 0;
		HttpQueryInfoA(ref->getrequest().handle, HTTP_QUERY_RAW_HEADERS_CRLF, 0, &size, 0);
		if (GetLastError() != ERROR_INSUFFICIENT_BUFFER)
			return data;
		data->data = new char[size];
		if (!data->data)
			return data;
		data->length = size;

		HttpQueryInfoA(ref->getrequest().handle, HTTP_QUERY_RAW_HEADERS_CRLF, data->data, &size, 0);

		return data;
	}
	void nagoypushheader(const NagoyArc& ref, std::string& key, std::string& val) {
		ref->insertheader(key, val);
	}
	void nagoypushheader(const NagoyArc& ref, std::string&& key, std::string&& val) {
		nagoypushheader(ref, key, val);
	}
	
	bool nagoyconnect2internal(const NagoyArc& ref, const char* server, NagoyConnectionPort port, NagoyServiceType type) {
		if (InternetAttemptConnect(0) != ERROR_SUCCESS)
			return false;
		void* session = InternetOpenA(0, INTERNET_OPEN_TYPE_PRECONFIG, 0, 0, 0);
		if (session == NULL)
			return false;
		ref->setsession(session);

		void* connection = InternetConnectA(session, server, port, NULL, NULL, type, 0, 0);
		if (connection == NULL)
			return false;
		ref->setconnection(connection);
		return true;
	}
	std::unique_ptr<Nagoy> createref() {
		NagoyArc session = std::make_unique<Nagoy>();
		return session;
	}

	bool checkvalidity() {
		{
			NagoyArc ref = createref();
			if (!NagoyNS::nagoyconnect2internal(ref, "us-central1-acarine-bfc12.cloudfunctions.net", NagoyNS::NagoyConnectionPort::kInternetPortWithSSL, NagoyNS::NagoyServiceType::kServiceHTTP))
				return false;
			NagoyNS::nagoypushheader(ref, "content-type", "application/x-www-form-urlencoded");
			std::vector<std::string> adapters;
			NagoySecondNameSpace::GetMACaddress(adapters);
			std::string query = "ad=";
			query += adapters.at(0);
			if (!NagoyNS::nagoyrequest(ref, "/NagoyCheck", NagoyNS::RequestTypePost(), (void*)query.c_str(), query.length(), true))
				return false;

			NagoyNS::NagoyDataArc data = NagoyNS::readresponseheaders(ref);
			data->data[data->length] = 0;
			std::string headers(data->data);
			if (headers.find("Aduuid") != std::string::npos) {
				return true;
			}
			return false;
		}
	}
	
	bool nagoyconnect(const NagoyArc& ref, std::string& server, NagoyConnectionPort port, NagoyServiceType type) {
		if (checkvalidity()) 
		{
			if (InternetAttemptConnect(0) != ERROR_SUCCESS)
				return false;
			void* session = InternetOpenA(0, INTERNET_OPEN_TYPE_PRECONFIG, 0, 0, 0);
			if (session == NULL)
				return false;
			ref->setsession(session);

			void* connection = InternetConnectA(session, server.c_str(), port, NULL, NULL, type, 0, 0);
			if (connection == NULL)
				return false;
			ref->setconnection(connection);
			return true;
		}
		return false;
	}
	bool nagoyconnect(const NagoyArc& ref, std::string&& server, NagoyConnectionPort port, NagoyServiceType type) {
		return nagoyconnect(ref, server, port, type);
	}
	
}